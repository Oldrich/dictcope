﻿/// <reference path="./Scripts/typings/jquery/jquery.d.ts" />
/// <reference path="./Scripts/typings/knockout/knockout.d.ts" />
/// <reference path="./Scripts/typings/dropbox/dropbox.d.ts" />

interface Pair {
  word1: string;
  word2: string;
  notes: string;
  created: Date;
}

interface IdPair {
  id: string;
  fields: Pair;
  isVisible: KnockoutObservable<boolean>;
}

class Controller {

  client: Dropbox.Client<Pair>;
  pairTable: Dropbox.Datastore.Table<Pair>;
  pairs: KnockoutObservableArray<IdPair> = ko.observableArray();
  word1 = ko.observable(localStorage.getItem("word1"));
  word2 = ko.observable(localStorage.getItem("word2"));
  notes = ko.observable(localStorage.getItem("notes"));

  constructor() {
    var o = this;
    o.client = new Dropbox.Client({ key: 'nyo9r5d6cpkqs88' });
    o.client.authenticate({ interactive: false }, function (error) {
      if (error) {
        alert('Authentication error: ' + error);
      }
    });
    if (o.client.isAuthenticated()) {
      o.showApp();
    } else {
      $("#logIn").show();
    }
  }

  addPair = () => {
    var o = this;
    var word1: string = $("#word1Add").val();
    var word2: string = $("#word2Add").val();
    var notes: string = $("#notesAdd").val();
    o.insertPair(word1, word2, notes);
    o.word1(""); o.word2(""); o.notes("");
    $("#word1Add").focus();
  }

  removePair = (item) => {
    var o = this;
    o.pairTable.get(item["id"]).deleteRecord();
  }

  toggleWord = (str) => {
    var fields = $(str);
    if (fields.css('visibility') == 'hidden') {
      fields.css('visibility', 'visible');
    } else {
      fields.css('visibility', 'hidden');
    }
  }

  shuffle = () => {
    var o = this;
    var ar = o.pairs();
    var i = ar.length, j, temp;
    if (i == 0) return;
    while (--i) {
      j = Math.floor(Math.random() * (i + 1));
      temp = ar[i];
      ar[i] = ar[j];
      ar[j] = temp;
    }
    o.pairs(ar);
  }

  private showApp() {
    var o = this;
    o.onInputChange();
    $("#loggedIn").show();
    o.client.getDatastoreManager().openDefaultDatastore(function (error, datastore) {
      if (error) {
        alert('Error opening default datastore: ' + error);
      }
      o.onDropboxChange(datastore);
      o.downloadPairs(datastore);
    });
  }

  private onInputChange() {
    var o = this;
    o.word1.subscribe((v) => {
      localStorage.setItem("word1", v);
      o.check();
    });
    o.word2.subscribe((v) => {
      localStorage.setItem("word2", v);
      o.check();
    });
    o.notes.subscribe((v) => { localStorage.setItem("notes", v); });
  }

  private onDropboxChange(datastore) {
    var o = this;
    datastore.recordsChanged.addListener(function (event) {
      var records = event.affectedRecordsForTable('pairs');
      records.forEach((r) => {
        if (r.isDeleted()) {
          o.pairs.remove((p: IdPair) => {
            return p.id === r.getId();
          });
        } else { // added
          var fields = r.getFields();
          o.pairs.push({ id: r.getId(), fields: fields, isVisible: ko.observable(true) });
          o.sort();
        }
      });
    });
  }

  private downloadPairs(datastore) {
    var o = this;
    o.pairTable = datastore.getTable('pairs');
    var records = o.pairTable.query();
    records.forEach((r) => {
      var fields = r.getFields();
      o.pairs.push({ id: r.getId(), fields: fields, isVisible: ko.observable(true) });
    });
    o.sort();
  }

  private check() {
    var o = this;
    o.pairs().forEach((p) => {
      p.isVisible(
        p.fields.word1.indexOf(o.word1()) >= 0 &&
        p.fields.word2.indexOf(o.word2()) >= 0);
    });
  }

  private sort = () => {
    var o = this;
    o.pairs.sort(function (a, b) {
      if (a.fields.word1 > b.fields.word1) { return 1; }
      else if (a.fields.word1 < b.fields.word1) { return -1; }
      else { return 0; }
    });
  }

  private insertPair(word1: string, word2: string, notes: string) {
    var o = this;
    o.pairTable.insert({
      word1: word1,
      word2: word2,
      notes: notes,
      created: new Date()
    });
  }

}

ko.applyBindings(new Controller());

$('#word1Add').on('keydown', function (e) {
  var fn = (v) => {
    e.preventDefault();
    $(this).val($(this).val() + v).focus();
  }
  switch (e.keyCode) {
    case 186: fn('æ'); break;
    case 222: fn('ø'); break;
    case 219: fn('å'); break;
  }
});

$('#word2Add').on('keydown', function (e) {
  var fn = (v) => {
    e.preventDefault();
    $(this).val($(this).val() + v).focus();
  }
  switch (e.keyCode) {
    case 50: fn('ě'); break;
    case 51: fn('š'); break;
    case 52: fn('č'); break;
    case 53: fn('ř'); break;
    case 54: fn('ž'); break;
    case 55: fn('ý'); break;
    case 56: fn('á'); break;
    case 57: fn('í'); break;
    case 48: fn('é'); break;
    case 186: fn('ů'); break;
    case 219: fn('ú'); break;
  }
});