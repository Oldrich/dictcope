﻿/// <reference path="./Scripts/typings/jquery/jquery.d.ts" />
/// <reference path="./Scripts/typings/knockout/knockout.d.ts" />
/// <reference path="./Scripts/typings/dropbox/dropbox.d.ts" />
var Controller = (function () {
    function Controller() {
        var _this = this;
        this.pairs = ko.observableArray();
        this.word1 = ko.observable(localStorage.getItem("word1"));
        this.word2 = ko.observable(localStorage.getItem("word2"));
        this.notes = ko.observable(localStorage.getItem("notes"));
        this.addPair = function () {
            var o = _this;
            var word1 = $("#word1Add").val();
            var word2 = $("#word2Add").val();
            var notes = $("#notesAdd").val();
            o.insertPair(word1, word2, notes);
            o.word1("");
            o.word2("");
            o.notes("");
            $("#word1Add").focus();
        };
        this.removePair = function (item) {
            var o = _this;
            o.pairTable.get(item["id"]).deleteRecord();
        };
        this.toggleWord = function (str) {
            var fields = $(str);
            if (fields.css('visibility') == 'hidden') {
                fields.css('visibility', 'visible');
            } else {
                fields.css('visibility', 'hidden');
            }
        };
        this.shuffle = function () {
            var o = _this;
            var ar = o.pairs();
            var i = ar.length, j, temp;
            if (i == 0)
                return;
            while (--i) {
                j = Math.floor(Math.random() * (i + 1));
                temp = ar[i];
                ar[i] = ar[j];
                ar[j] = temp;
            }
            o.pairs(ar);
        };
        this.sort = function () {
            var o = _this;
            o.pairs.sort(function (a, b) {
                if (a.fields.word1 > b.fields.word1) {
                    return 1;
                } else if (a.fields.word1 < b.fields.word1) {
                    return -1;
                } else {
                    return 0;
                }
            });
        };
        var o = this;
        o.client = new Dropbox.Client({ key: 'nyo9r5d6cpkqs88' });
        o.client.authenticate({ interactive: false }, function (error) {
            if (error) {
                alert('Authentication error: ' + error);
            }
        });
        if (o.client.isAuthenticated()) {
            o.showApp();
        } else {
            $("#logIn").show();
        }
    }
    Controller.prototype.showApp = function () {
        var o = this;
        o.onInputChange();
        $("#loggedIn").show();
        o.client.getDatastoreManager().openDefaultDatastore(function (error, datastore) {
            if (error) {
                alert('Error opening default datastore: ' + error);
            }
            o.onDropboxChange(datastore);
            o.downloadPairs(datastore);
        });
    };

    Controller.prototype.onInputChange = function () {
        var o = this;
        o.word1.subscribe(function (v) {
            localStorage.setItem("word1", v);
            o.check();
        });
        o.word2.subscribe(function (v) {
            localStorage.setItem("word2", v);
            o.check();
        });
        o.notes.subscribe(function (v) {
            localStorage.setItem("notes", v);
        });
    };

    Controller.prototype.onDropboxChange = function (datastore) {
        var o = this;
        datastore.recordsChanged.addListener(function (event) {
            var records = event.affectedRecordsForTable('pairs');
            records.forEach(function (r) {
                if (r.isDeleted()) {
                    o.pairs.remove(function (p) {
                        return p.id === r.getId();
                    });
                } else {
                    var fields = r.getFields();
                    o.pairs.push({ id: r.getId(), fields: fields, isVisible: ko.observable(true) });
                    o.sort();
                }
            });
        });
    };

    Controller.prototype.downloadPairs = function (datastore) {
        var o = this;
        o.pairTable = datastore.getTable('pairs');
        var records = o.pairTable.query();
        records.forEach(function (r) {
            var fields = r.getFields();
            o.pairs.push({ id: r.getId(), fields: fields, isVisible: ko.observable(true) });
        });
        o.sort();
    };

    Controller.prototype.check = function () {
        var o = this;
        o.pairs().forEach(function (p) {
            p.isVisible(p.fields.word1.indexOf(o.word1()) >= 0 && p.fields.word2.indexOf(o.word2()) >= 0);
        });
    };

    Controller.prototype.insertPair = function (word1, word2, notes) {
        var o = this;
        o.pairTable.insert({
            word1: word1,
            word2: word2,
            notes: notes,
            created: new Date()
        });
    };
    return Controller;
})();

ko.applyBindings(new Controller());

$('#word1Add').on('keydown', function (e) {
    var _this = this;
    var fn = function (v) {
        e.preventDefault();
        $(_this).val($(_this).val() + v).focus();
    };
    switch (e.keyCode) {
        case 186:
            fn('æ');
            break;
        case 222:
            fn('ø');
            break;
        case 219:
            fn('å');
            break;
    }
});

$('#word2Add').on('keydown', function (e) {
    var _this = this;
    var fn = function (v) {
        e.preventDefault();
        $(_this).val($(_this).val() + v).focus();
    };
    switch (e.keyCode) {
        case 50:
            fn('ě');
            break;
        case 51:
            fn('š');
            break;
        case 52:
            fn('č');
            break;
        case 53:
            fn('ř');
            break;
        case 54:
            fn('ž');
            break;
        case 55:
            fn('ý');
            break;
        case 56:
            fn('á');
            break;
        case 57:
            fn('í');
            break;
        case 48:
            fn('é');
            break;
        case 186:
            fn('ů');
            break;
        case 219:
            fn('ú');
            break;
    }
});
//# sourceMappingURL=controller.js.map
