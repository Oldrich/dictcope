declare module Dropbox {
  class Client<T> {
    constructor(options: ClientOptions);
    authenticate(options: { interactive: boolean; }, callback): Client<T>;
    authenticate(): Client<T>;
    isAuthenticated(): boolean;
    signOut(options: { mustInvalidate: boolean; }, callback): XMLHttpRequest;
    getDatastoreManager(): Datastore.DatastoreManager<T>;
  }

  interface ClientOptions {
    key: string;
    secret?: string;
    token?: string;
    uid?: string;
  }

  interface ApiError {
  }

  interface Datastore<T> {
    recordsChanged: Util.EventSource<Datastore.RecordsChanged<T>>;
    getTable(tableId: string): Datastore.Table<T>;
    close(): void;
    getId(): string;
    getSyncStatus(): Object;
  }

  module Datastore {
    class DatastoreManager<T> {
      openDefaultDatastore(callback: (error: ApiError, datastore: Datastore<T>) => void): void;
    }

    class Table<T> {
      isValidId(tableId: string): boolean;
      getId(): string;
      get(recordId: string): Record<T>;
      getOrInsert(recordId: string, defaultValues: T): Record<T>;
      insert(fieldValues: T): Record<T>;
      query(): Array<Record<T>>;
      query(fieldValues: T): Array<Record<T>>;
      setResolutionRule(fieldName: string, rule: string): Table<T>;
    }

    interface Record<T> {
      deleteRecord(): Record<T>;
      get(fieldName: string);
      getFields(): T;
      getId(): string;
      getTable(): Table<T>;
      hasField(fieldName: string): boolean;
      isDeleted(): boolean;
      set(fieldName: string, value): Record<T>;
      update(a: Object): Record<T>;
    }

    module Record {
      function isValidId(recordId: string): boolean;
    }

    interface RecordsChanged<T> {
      affectedRecordsByTable(): Object;
      affectedRecordsForTable(tableId: string): Array<Record<T>>;
      isLocal(): boolean;
    }

  }

  module Util {

    interface EventSource<T> {
      constructor(options: { cancelable: boolean; });
      addListener(listener: (event: T) => any): EventSource<T>;
      removeListener(listener: (event: T) => any): EventSource<T>;
      dispatch(event: T): boolean;
    }

  }

}